# BeetBot Web UI
This repo houses the backend and frontend code for the BeetBot web ui.

The webui displays the modmail that has been received through the bot.

It uses the discord oauth2 flow described [here](https://discord.com/developers/docs/topics/oauth2)
