FROM python:3.9

COPY ./src/requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt
COPY ./src/ /app
WORKDIR /app
ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:5000", "wsgi:app"]