from logging import error
from re import I
import uuid
from flask import (
    Flask,
    request,
    session,
    Response,
    redirect,
    render_template,
    url_for)
import pymongo
import requests
import secrets
import os
import sys
import time
"""Backend code for the BeetBot Web UI.

This file has flask routes for handling rendering the web UI, authorization
towards the discord api, and managing bot settings. It requires that the
environment variable `BEETBOT_CONNECTION_STRING` is defined.


Attributes:
    CONNECTION_STRING (str): MongoDB connection string
    app (flask.Flask): Flask instance


"""

tickets = [
    {
        "userId": "514738328051122187",
        "message": "@vorap called me a bitchboy",
        "ticketId": uuid.uuid4().hex,
        "replies": [],
        "timestamp": int(time.time())
    },
    {
        "userId": "514738328051122187",
        "message": "@vorap called me a bitchboy",
        "ticketId": uuid.uuid4().hex,
        "replies": [],
        "timestamp": int(time.time())
    },
]

sessions = {}

API_ENDPOINT = "https://discord.com/api/v8"
REFRESH_TOKEN_TIMEOUT = 86400

try:
    CONNECTION_STRING = os.environ["BEETBOT_CONNECTION_STRING"]
    FLASK_SECRET_KEY = os.environ["FLASK_SECRET_KEY"]
    DISCORD_CLIENT_ID = os.environ["DISCORD_CLIENT_ID"]
    DISCORD_CLIENT_SECRET = os.environ["DISCORD_CLIENT_SECRET"]
    REDIRECT_URI = os.environ["REDIRECT_URI"]
    DISCORD_BOT_TOKEN = os.environ["DISCORD_BOT_TOKEN"]
    DISCORD_ROLE_ID = os.environ["DISCORD_BOT_TOKEN"]
    DISCORD_GUILD_ID = os.environ["DISCORD_GUILD_ID"]

except NameError as e:
    print(f"Missing environment variable {e}")
    sys.exit()

app = Flask(__name__)
app.secret_key = FLASK_SECRET_KEY


def logged_in(session):
    if token := session.get("TOKEN"):
        if token in sessions:
            return True
    return False
    

def error_not_authorized():
    return {"error": "401 Unauthorized"}


def get_other_user_info(user_id):
    header = {"Authorization": f"Bot {DISCORD_BOT_TOKEN}"}
    user_info = requests.get(
        f"{API_ENDPOINT}/users/{user_id}", headers=header).json()
    return {"user": user_info}


def get_current_user_info(session_token):
    _session = sessions[session_token]
    header = {"Authorization": f"Bearer {_session['access_token']}"}
    user_info = requests.get(
        f"{API_ENDPOINT}/users/@me", headers=header).json()
    guilds = requests.get(
        f"{API_ENDPOINT}/users/@me/guilds", headers=header).json()
    return {"user": user_info, "guilds": guilds}


def get_guild_member(guild_id, user_id):
    header = {"Authorization": f"Bot {DISCORD_BOT_TOKEN}"}
    member_info = requests.get(
        f"{API_ENDPOINT}/guilds/{guild_id}/members/{user_id}", headers=header).json()
    return {"member": member_info}

def exchange_code(code):
    data = {
            "client_id": DISCORD_CLIENT_ID,
            "client_secret": DISCORD_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": REDIRECT_URI
        }
    r = requests.post(
        f"{API_ENDPOINT}/oauth2/token",
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data=data
        )
    r.raise_for_status()
    return r.json()


@app.route("/")
def index():
    """The main route, which will redirect you to the login page or the bot ui.

    The redirect will happen depending on what the users session object looks
    like.

    Args:
        None
    
    Returns:
        redirect: A flask redirect, which throws the user to the login page or
            to the main dashboard.


    """
    if logged_in(session):
        return redirect(url_for("main_page"))
    return redirect(url_for("frnt_login"))


@app.route("/login")
def frnt_login():
    """Renders the login page for authorizing to the discord api.

    Returns:
        str: A rendered html template.
    """
    return render_template("/login.html")


@app.route("/api/v1/login")
def back_login():
    code = request.args.get("code")
    state = request.args.get("state")
    if not code or not state or not state == session.get("TOKEN"):
        return redirect(url_for("frnt_login"))
    resp = exchange_code(code)
    sessions[session["TOKEN"]] = {
        "access_token": resp["access_token"],
        "refresh_token": resp["refresh_token"],
        "token_expires": time.time() - resp["expires_in"],
        "logged_in": True
    }
    return render_template("/closePopup.html")


@app.route("/api/v1/token")
def back_token():
    """Returns the users session token"""
    if not session.get("TOKEN"):
        session["TOKEN"] = secrets.token_urlsafe(16)
    return {"session_token": session["TOKEN"]}


@app.route("/app/modmail")
def main_page():
    """Renders the modmail template if the user is logged in"""
    if logged_in(session):
        return render_template("/app/modmail.html")
    return redirect(url_for("frnt_login"))


@app.route("/api/v1/user/<user_id>")
def get_user(user_id):

    token = session.get("TOKEN")
    if not logged_in(session):
        return error_not_authorized()

    if(user_id == "@me"):
        try:
            data = get_current_user_info(token)
            if isinstance(data, str):
                return {
                    "status": "400 Error",
                    "message": data
                }
            return {
                "status": "200 OK",
                "data": data
            }
        except Exception as e:
            return {
                "status": "500 Internal Server Error",
                "message": e
            }
    else:
        try:
            return {
                "status": "200 OK",
                "data": get_other_user_info(user_id)
            }
        except Exception as e:
            return {
                "status": "500 Internal Server Error",
                "message": "bruh"
            }


@app.route("/api/v1/tickets")
def enum_tickets():
    if sessions[session["TOKEN"]].get("logged_in"):
        return {"status": "200 OK", "data": tickets}
    return error_not_authorized()


@app.route("/api/v1/logout")
def logout():
    del sessions[session["TOKEN"]]
    session.clear()
    return redirect(url_for("frnt_login"))

